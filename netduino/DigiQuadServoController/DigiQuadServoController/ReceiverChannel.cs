using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;

namespace DigiQuadServoController
{
	/// <summary>
	/// Class that allows for making a fake receiver channel
	/// </summary>
	public class ReceiverChannel
	{
		public double Value
		{
			get
			{
				return this.outputValue;
			}
			set
			{
				this.SetValue(value);
			}
		}

		/// <summary>
		/// Receiver output value. 0.0 = "Low PWM", 100.0 is "High PWM".
		/// </summary>
		private double outputValue;

		/// <summary>
		/// Actual PWM pin that this class will use.
		/// </summary>
		private PWM pwm = null;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="pin">PWM enabled pin on the netduino</param>
		public ReceiverChannel(Cpu.Pin pin)
		{
			// Create pwm instance
			pwm = new PWM(pin);

			// Set value to middle by default
			this.SetValue(50.0);
		}

		/// <summary>
		/// Sets the value for the receiver output
		/// </summary>
		/// <param name="value"></param>
		private void SetValue(double value)
		{
			uint valInt = (uint)System.Math.Round(value * 10);

			// Set new pwm
			this.pwm.SetPulse(20000, 1000 + valInt);

			// Save speed value
			this.outputValue = value;
		}
	}
}

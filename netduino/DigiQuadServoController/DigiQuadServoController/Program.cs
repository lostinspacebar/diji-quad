﻿using System;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;
using System.IO.Ports;

namespace DigiQuadServoController
{
	public class Program
	{
		/// <summary>
		/// Receiver channel instances
		/// </summary>
		static ReceiverChannel[] receiverChannels = new ReceiverChannel[4];

		/// <summary>
		/// Serial port 
		/// </summary>
		static SerialPort serialPort = null;

		/// <summary>
		/// Message buffer
		/// </summary>
		static byte[] messageBuffer = new byte[128];

		/// <summary>
		/// Length of the current message being received
		/// </summary>
		static int messageBufferLength = 0;

		/// <summary>
		/// Main
		/// </summary>
		public static void Main()
		{
			// Initialize channel instances
			receiverChannels[0] = new ReceiverChannel(Pins.GPIO_PIN_D5);
			receiverChannels[1] = new ReceiverChannel(Pins.GPIO_PIN_D6);
			receiverChannels[2] = new ReceiverChannel(Pins.GPIO_PIN_D9);
			receiverChannels[3] = new ReceiverChannel(Pins.GPIO_PIN_D10);

			// Initialize serial port
			serialPort = new SerialPort("COM1");
			serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
			serialPort.Open();
		}

		/// <summary>
		/// Parse a message over the serial port
		/// </summary>
		/// <param name="message"></param>
		private static void ParseMessage(byte[] message)
		{
			// Find message type
			uint messageType = (uint)message[2] | (((uint)message[3]) << 8);

			// Parse values
			int numValues = (message.Length - 6) / 2;
			uint[] values = new uint[numValues];
			int count = 0;
			for (int i = 3; i < message.Length - 2; i += 2)
			{
				values[count++] = (uint)message[i] | (((uint)message[i + 1]) << 8);
			}

			// Handle message
			HandleMessage(messageType, values, message);
		}

		/// <summary>
		/// Handle message with values
		/// </summary>
		/// <param name="type"></param>
		/// <param name="values"></param>
		/// <param name="message"></param>
		private static void HandleMessage(uint type, uint[] values, byte[] message)
		{
			// Control values
			if (type == 0x34)
			{
				// Value 0 is roll
				receiverChannels[0].Value = ((double)values[0]) / 10.0;

				// Value 1 is pitch
				receiverChannels[1].Value = ((double)values[1]) / 10.0;

				// Value 2 is throttle
				receiverChannels[2].Value = ((double)values[2]) / 10.0;

				// Value 3 is yaw
				receiverChannels[3].Value = ((double)values[3]) / 10.0;
			}
		}

		/// <summary>
		/// Handle data from the wifi
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		static void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			if (serialPort.BytesToRead > 0)
			{
				// Read in a single byte
				byte[] buffer = new byte[1];
				int bytesRead = serialPort.Read(buffer, 0, 1);

				// Make sure we got 1 byte
				if (bytesRead == 1)
				{
					var b = buffer[0];
					
					// Look for start byte
					if (messageBufferLength < 2)
					{
						if (b == 0xFF)
						{
							// Proceed with message
							messageBuffer[messageBufferLength++] = b;
						}
						else
						{
							// Reset message
							messageBufferLength = 0;
						}
					}
					else
					{
						messageBuffer[messageBufferLength++] = b;

						// Check for end byte
						if (messageBuffer[messageBufferLength - 1] == 0xFE && messageBuffer[messageBufferLength - 2] == 0xFF)
						{
							// Got a full message
							byte[] message = new byte[messageBufferLength];
							messageBuffer.CopyTo(message, 0);
							ParseMessage(message);
						}
					}
				}
			}
		}
	}
}
